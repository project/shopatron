<?php

/**
 * @file
 * Administrative forms and callbacks for the Shopatron module.
 */


/**
 * Build the administrator settings form for Shopatron.
 */
function shopatron_settings_form() {
  $form = array();

  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('Connection settings'),
    '#collapsible' => TRUE,
    '#collapsed' => variable_get('shopatron_mfg_id', '') != '',
  );
  $form['api']['shopatron_mfg_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Manufacturer ID'),
    '#description' => t('Supplied by your implementation coordinator.'),
    '#default_value' => variable_get('shopatron_mfg_id', ''),
    '#size' => 16,
  );
  $form['api']['shopatron_catalog_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Catalog ID'),
    '#description' => t('Unless you have configured multiple catalogs, leave set to 0.') .'<br />'. t('Supplied by your implementation coordinator.'),
    '#default_value' => variable_get('shopatron_catalog_id', '0'),
    '#size' => 16,
  );
  $form['api']['shopatron_protocol'] = array(
    '#type' => 'select',
    '#title' => t('Connection protocol'),
    '#options' => array(
      'https' => 'HTTPS',
      'http' => 'HTTP',
    ),
    '#default_value' => variable_get('shopatron_protocol', 'https'),
  );
  $form['api']['shopatron_logging'] = array(
    '#type' => 'radios',
    '#title' => t('Export logging'),
    '#options' => array(
      0 => t('Log nothing'),
      1 => t('Log failed exports'),
      2 => t('Log successful exports'),
      3 => t('Log failed and successful exports'),
    ),
    '#default_value' => variable_get('shopatron_logging', 1),
  );
  $form['api']['shopatron_failure'] = array(
    '#type' => 'radios',
    '#title' => t('Export failure behavior'),
    '#description' => t('According to specifications, order data should be e-mailed to Shopatron for support and manual creation.'),
    '#options' => array(
      'mail_shopatron' => t('E-mail customer and order data to Shopatron.'),
      'mail_both' => t('E-mail customer and order data to Shopatron and the admin e-mail below.'),
      'mail_admin' => t('E-mail customer and order data to the admin e-mail below.'),
    ),
    '#default_value' => variable_get('shopatron_failure', 'mail_shopatron'),
  );
  $form['api']['shopatron_admin_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Shopatron admin e-mail'),
    '#default_value' => variable_get('shopatron_admin_email', ''),
    '#size' => 32,
  );

  // Autodetect compatible cart/redirect systems.
  $form['integration'] = array(
    '#type' => 'fieldset',
    '#title' => t('Shopping cart integration'),
    '#description' => t('The Shopatron module will automatically detect and provide integration for available shopping cart systems on your Drupal site.'),
    '#collapsible' => TRUE,
    '#collapsed' => variable_get('shopatron_ubercart', FALSE),
  );

  // Add Ubercart support.
  if (module_exists('uc_cart')) {
    $form['integration']['shopatron_ubercart'] = array(
      '#type' => 'checkbox',
      '#title' => t('Redirect the Ubercart checkout form to Shopatron.'),
      '#description' => t('Enabling this integration will render the normal Ubercart checkout inoperable. Orders will not be recorded locally.'),
      '#default_value' => variable_get('shopatron_ubercart', FALSE),
    );
  }

  // Display a message if no compatible carts were found.
  if (count(element_children($form['integration'])) == 0) {
    $form['integration']['n/a'] = array(
      '#value' => '<div>'. t('Consult the <a href="!shopatron_project_page">Shopatron</a> documentation for information on supported shopping cart systems.', array('!shopatron_project_page' => url('http://drupal.org/project/shopatron'))) .'</div>',
    );
  }

  $form['items'] = array(
    '#type' => 'fieldset',
    '#title' => t('Product export settings'),
    '#description' => t('These settings affect values that are used when adding products for export to Shopatron. Unless otherwise noted, these settings are global and may not be altered per-product.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['items']['shopatron_item_product_id'] = array(
    '#type' => 'radios',
    '#title' => t('Product ID'),
    '#options' => array(
      'model' => t('Use the product model/SKU.'),
      // 'nid' => t('Use the node ID.'), // Currently throws an error.
    ),
    '#default_value' => variable_get('shopatron_item_product_id', 'model'),
  );
  $form['items']['shopatron_item_price'] = array(
    '#type' => 'radios',
    '#title' => t('Manufacturers price'),
    '#options' => array(
      'list' => t('List price'),
      'price' => t('Sell price'),
      'cost' => t('Cost'),
    ),
    '#default_value' => variable_get('shopatron_item_price', 'list'),
  );
  $form['items']['shopatron_item_avg_margin'] = array(
    '#type' => 'textfield',
    '#title' => t('Average margin'),
    '#description' => t('The average margin of the product (for example, .5 for 50%).') .'<br />'. t('Do not include the %.'),
    '#default_value' => variable_get('shopatron_item_avg_margin', '.5'),
    '#size' => 16,
  );
  $form['items']['shopatron_item_availability'] = array(
    '#type' => 'radios',
    '#title' => t('Product availability'),
    '#options' => array(
      'Y' => t('Y - Available for fulfillment.'),
      'M' => t('M - Assign to the manufacturer for fulfillment.'),
    ),
    '#default_value' => variable_get('shopatron_item_availability', 'Y'),
  );
  $form['items']['shopatron_item_weight'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include the product weight in the export.'),
    '#description' => t('Do not include them if weights on your site are not in pounds.'),
    '#default_value' => variable_get('shopatron_item_weight', TRUE),
  );

  return system_settings_form($form);
}

function shopatron_settings_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['shopatron_admin_email']) && !valid_email_address($form_state['values']['shopatron_admin_email'])) {
    form_set_error('shopatron_admin_email', t('You have specified an invalid admin e-mail address.'));
  }
}
