<?php

/**
 * @file
 * Page forms and callbacks for the Shopatron module.
 */


/**
 * Build the Shopatron checkout support e-mail form.
 */
function shopatron_checkout_support_form() {
  $form = array();

  $items = uc_cart_get_contents();

  // If the cart is empty, do not allow support.
  if (empty($items)) {
    drupal_set_message(t('We do not offer checkout support unless you have items in your shopping cart.'), 'error');
    drupal_goto('cart');
  }

  $form['items'] = array(
    '#type' => 'value',
    '#value' => $items,
  );

  $form['first_name'] = array(
    '#type' => 'textfield',
    '#title' => t('First name'),
    '#required' => TRUE,
    '#size' => 32,
  );
  $form['last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last name'),
    '#required' => TRUE,
    '#size' => 32,
  );
  $form['phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone number'),
    '#required' => TRUE,
    '#size' => 32,
  );
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail address'),
    '#required' => TRUE,
    '#size' => 32,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit order'),
  );

  return $form;
}

function shopatron_checkout_support_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['email'])) {
    form_set_error('email', t('Please provide a valid e-mail address.'));
  }
}

function shopatron_checkout_support_form_submit($form, &$form_state) {
  global $user;

  // Determine the appropriate recipients for the e-mail.
  $recipients = array();

  $on_failure = variable_get('shopatron_failure', 'mail_shopatron');

  if ($on_failure == 'mail_shopatron' || $on_failure == 'mail_both') {
    $recipients[] = 'support@shopatron.com';
  }

  if ($on_failure == 'mail_both' || $on_failure == 'mail_admin') {
    $admin = variable_get('shopatron_admin_email', '');

    if (valid_email_address($admin)) {
      $recipients[] = $admin;
    }
  }

  // Use the Shopatron e-mail as the from address if set.
  $from = variable_get('shopatron_admin_email', '');

  if (empty($from) || !valid_email_address($from)) {
    $from = uc_store_email_from();
  }

  // Send the e-mail to the recipients.
  foreach ($recipients as $to) {
    $sent = drupal_mail('shopatron', 'checkout-support', $to, language_default(), $form_state['values'], $from);

    // Log the e-mail if sending failed.
    if (!$sent['result']) {
      watchdog('shopatron', 'Attempt to e-mail @email the following Shopatron e-mail failed. !body', array('@email' => $to, '!body' => $sent['body']), WATCHDOG_ERROR);
    }
  }

  // Empty the shopping cart.
  uc_cart_empty(uc_cart_get_id());

  // Log the form submission.
  watchdog('shopatron', 'Checkout support form submitted by @user as @first @last, @email', array('@user' => $user->uid ? $user->name : t('anonymous user'), '@first' => $form_state['values']['first_name'], '@last' => $form_state['values']['last_name'], '@email' => $form_state['values']['email']), WATCHDOG_NOTICE, l(t('view user'), 'user/'. $user->uid));

  // Display a message and send the user to the front page.
  drupal_set_message(t('Your order has been submitted. A support agent will contact you as soon as possible.'));

  $form_state['redirect'] = '<front>';
}
